---
Week: 37
Content:  Introduction to programming and Python
Material: See links in weekly plan
Initials: NISI
---

# Week 37 ITT1-programming - Introduction

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Python installed and tested
* Read chapter 1 in Python For Everybody
* Python For Everybody - Chapter 1 exercises completed 

### Learning goals

The student knows:

* What a program is
* What Python REPL is and how to use it
* What Python IDLE is and how to use it
* How to document using Gitlab

## Deliverables

* Gitlab exercises project created
* Chapter 1 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Thursday 2020-09-10 (B class) and Friday 2020-09-11 (A class)  
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the course
    * Introduction slides [https://docs.google.com/presentation/d/10VGoqXbXV6zunfk6lKmUuvXKWMQnGgU6bXgEYwODpYU/edit?usp=sharing](https://docs.google.com/presentation/d/10VGoqXbXV6zunfk6lKmUuvXKWMQnGgU6bXgEYwODpYU/edit?usp=sharing)
    
* 9:30 Preparation for exercises
    Either/or:
    * Read chapter 1 in Python For Everybody
    * Watch chapter 1 videos 
* 10:00 Visit from the study counseller Hanne (only Thursday)
* 10:45 Hands-on time
* 12:15 Lunch break
* 13:00 Hands-on time
* 13:45 Evaluation of the day + Q&A  
        To help you reflect on your own learning and give feedback to improve the lectures, answer the reflection questions at [https://forms.gle/PF5YX6SyrMgCXQg56](https://forms.gle/PF5YX6SyrMgCXQg56)
* 14:00 Hands-on time 
* 16:15 End of day

## Hands-on time

* Exercise 0: Install Python + Hello World  
For details see the exercise document at [https://eal-itt.gitlab.io/20a-itt1-programming](https://eal-itt.gitlab.io/20a-itt1-programming)

* Exercise 1: Gitlab project for exercises hand-in  
You need to setup your gitlab project in the gitlab group [https://gitlab.com/20a-itt1-programming-exercises](https://gitlab.com/20a-itt1-programming-exercises)  
Ask NISI or another admin for access to the group, you need to supply your gitlab @username (first 3 students will be appointed as admins in the group)   
If needed recap how to setup GIT and Gitlab: [https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0](https://npes.gitlab.io/claat-generator/gitlab_daily_workflow/index.html#0)

* Exercise 2: Complete Python For Everybody, chapter 1 exercises and document it in a .py file on gitlab  
Exercises are in chapter 1 of the PY4E book

## Comments

PY4E Chapter 1 video lessons:

* part 1 [https://youtu.be/fvhNadKjE8g](https://youtu.be/fvhNadKjE8g)
* part 2 [https://youtu.be/VQZTZsXk8sA](https://youtu.be/VQZTZsXk8sA)
* part 3 [https://youtu.be/LLzFNlCjTSo](https://youtu.be/LLzFNlCjTSo)
* part 4 [https://youtu.be/gsry2SYOFCw](https://youtu.be/gsry2SYOFCw)
