---
Week: 48
Content:  Regular Expressions
Material: See links in weekly plan
Initials: NISI
---

# Week 48 ITT1-programming - Regular Expressions

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Solve PY4E chapter 11 exercises

### Learning goals

The student can:

* Use regular expressions

The student knows: 

* Basic syntax of regular expressions
* When to use regular expressions

## Deliverables

* Chapter 11 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Friday 2020-11-27 (A + B class)  
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review  
    Students shows how they solved chapter 10 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 11
    * Watch chapter 11 videos 
* 10:30 Hands-on time
* 12:15 Lunch break
* 13:00 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 11 video lessons:

* part 1 [https://youtu.be/ovZsvN67Glc](https://youtu.be/ovZsvN67Glc)
* part 2 [https://youtu.be/fiar4QZZ7Xo](https://youtu.be/fiar4QZZ7Xo)
* part 3 [https://youtu.be/GiQdXo2Bvgc](https://youtu.be/GiQdXo2Bvgc)

RegEx online parser and bulder:

* [regex101.com](https://regex101.com/)  
  More exists online, the linked one might not be the best of them ?
