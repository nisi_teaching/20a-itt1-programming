---
Week: 51
Content:  Object-oriented programming
Material: See links in weekly plan
Initials: NISI
---

# Week 51 ITT1-programming - Object-oriented programming

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Object-oriented programming exercises 

### Learning goals

The student can:

* Create simple classes
* Instantiate objects from classes
* Extend classes

The student knows: 

* What a class is
* Why classes are useful
* Class attributes
* Class methods
* Class constructors
* The ```self``` keyword

## Deliverables

* Chapter 14 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Friday 2021-12-18 (A class and B class)
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review  
    Students shows how they solved chapter 13 exercises from py4e 
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 14
    * Watch chapter 14 videos 
* 10:15 Video: Python Classes and Objects [https://youtu.be/apACNr7DC_s](https://youtu.be/apACNr7DC_s)
* 10:30 Live examples
* 11:00'ish Hands-on time
* 12:15 Lunch break
* 13:00 Hands-on time
* 16:15 End of day

## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 14 video lessons:

* part 1 [https://youtu.be/u9xZE5t9Y30](https://youtu.be/u9xZE5t9Y30)
* part 2 [https://youtu.be/b2vc5uzUfoE](https://youtu.be/b2vc5uzUfoE)

