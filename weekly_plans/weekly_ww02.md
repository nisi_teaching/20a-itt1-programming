---
Week: 02
Content:  Recap and evaluation
Material: See links in weekly plan
Initials: NISI
---

# Week 02 ITT1-programming - Recap and evaluation

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Complete exercises from the semester you haven't completed 

### Learning goals

* None

## Deliverables

* Exercises completed and documented on gitlab

## Schedule

Thursday 2021-01-14 (B class) and Friday 2021-01-15 (A class)
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Hands-on time - Python quiz individual
* 9:30 Recap Q&A based on your questions
* 10:30'ish Evaluation  
    Please fill out this survey [https://forms.gle/Qki6BhVG2NLwVctd9](https://forms.gle/Qki6BhVG2NLwVctd9)
* 12:15 Lunch break
* 13:00 Catchup on exercises, prepare for exam
* 15:30 End of programming course

## Hands-on time

* Exercise 0 - Python quiz

* Complete exercises from the semester you haven't completed

## Comments

* This week is without lecturer

