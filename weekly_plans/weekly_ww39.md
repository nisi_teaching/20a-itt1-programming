---
Week: 39
Content:  Conditionals
Material: See links in weekly plan
Initials: NISI
---

# Week 39 19A-ITT1-programming - Conditionals

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 3 in Python For Everybody
* Chapter 3 exercises, in Python For Everybody, completed 

### Learning goals

The student can implement:

* Conditional execution
* logical operators
* Exception handling (try/except)
* Flowcharts

## Deliverables

* Chapter 3 exercises, in Python For Everybody, documented on Gitlab

## Schedule

Wednesday 2020-09-23 (A + B class)  
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review  
    Students shows how they solved chapter 2 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 3
    * Watch chapter 3 videos 
* 10:15 Hands-on time - Exercise 0 & 1
* 11:15 Flowchart presentations
* 11:45 Hands-on time - Exercise 2
* 12:15 Lunch break
* 13:00 Hands-on time - exercise 3 & 4    
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.

## Comments

PY4E Chapter 3 video lessons:

* part 1 [https://youtu.be/2aA3VBdcl6A](https://youtu.be/2aA3VBdcl6A)
* part 2 [https://youtu.be/OczkNrHPBps](https://youtu.be/OczkNrHPBps)

Socratica videos covering some of the topics from chapter 2 and 3:

* [If, Then, Else in Python](https://youtu.be/f4KOjWS_KZs)
* [Exceptions in Python](https://youtu.be/nlCKrKGHSSk)