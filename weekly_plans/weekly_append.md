
# Additional resources

## Communication

* [https://riot.im/](https://riot.im/) in the room called ‘UCL ITT 20A’

* Lecture plan, weekly plans, exercises [https://eal-itt.gitlab.io/20a-itt1-programming/](https://eal-itt.gitlab.io/20a-itt1-programming)

* Exercises hand-in: Your gitlab projects, In the gitlab group [https://gitlab.com/20a-itt1-programming-exercises](https://gitlab.com/20a-itt1-programming-exercises)

* Not able to attend class - write to nisi@ucl.dk or use Riot

* Everything else https://ucl.itslearning.com 


## Expectations

* Show up on time
* Notify nisi@ucl.dk if you are unable to attend or late for class
* Complete all reading, exercises and OLA’s
* Practice, practice, practice
* Be curious

## Sample code from Charles Severance (PY4E Author)

Everything in a .zip file [https://www.py4e.com/code3.zip](https://www.py4e.com/code3.zip)  
Individual files [https://www.py4e.com/code3/](https://www.py4e.com/code3/)

## Sample code from Nikolaj Simonsen (incomplete)

[https://gitlab.com/EAL-ITT/20a-itt1-programming/tree/master/docs/nikolaj_simonsen_programming_exercises](https://gitlab.com/EAL-ITT/20a-itt1-programming/tree/master/docs/nikolaj_simonsen_programming_exercises)