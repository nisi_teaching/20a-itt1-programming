---
Week: 44
Content:  Files
Material: See links in weekly plan
Initials: NISI
---

# Week 44 ITT1-programming - Files

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Read chapter 7 in Python For Everybody
* Solve PY4E chapter 7 exercises using pair programming 

### Learning goals

The student can:

* Open/read/write/close files
* Parse files
* Handle file exceptions 

## Deliverables

* Chapter 7 exercises, in Python For Everybody, documented on Gitlab
* A gitlab project created with a readme.md explaining your own challenge

## Schedule

Thursday 2020-10-29 (B class) and Friday 2020-10-30 (A class)
Online at zoom: [https://ucldk.zoom.us/j/66008815483](https://ucldk.zoom.us/j/66008815483) password: 1234  
*Please have your camera turned on and your microphone turned off*

* 9:00 Introduction to the day 
* 9:15 Code review  
    Students shows how they solved the number guessing game or chapter 6 exercises from py4e
* 9:45 Preparation for hands-on time  
    Either/or:
    * Read chapter 7
    * Watch chapter 7 videos 
* 10:30 Hands-on time
* 12:15 Lunch break
* 12:15 Hands-on time
* 16:15 End of day


## Hands-on time

See [https://eal-itt.gitlab.io/20a-itt1-programming/exercises/](https://eal-itt.gitlab.io/20a-itt1-programming/exercises/) for details.


## Comments

PY4E Chapter 7 video lessons:

* part 1 [https://youtu.be/9KJ-XeQ6ZlI](https://youtu.be/9KJ-XeQ6ZlI)
* part 2 [https://youtu.be/0t4rvnySKR4](https://youtu.be/0t4rvnySKR4)

pycharm virtual environment [https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html](https://www.jetbrains.com/help/pycharm/creating-virtual-environment.html)