"""
Exercise 3: Sometimes when programmers get bored or want to have a bit of fun, they add a harmless Easter Egg
to their program.
Modify the program that prompts the user for the file name so that it prints a funny message when the user types
in the exact file name “na na boo boo”.
The program should behave normally for all other files which exist and don’t exist.
Here is a sample execution of the program:

python egg.py
Enter the file name: mbox.txt
There were 1797 subject lines in mbox.txt

python egg.py
Enter the file name: missing.tyxt
File cannot be opened: missing.tyxt

python egg.py
Enter the file name: na na boo boo
NA NA BOO BOO TO YOU - You have been punk'd!
"""

file_name = input('Enter a file name: ')

if file_name == "na na boo boo":
    print("NA NA BOO BOO TO YOU - You have been punk'd!")

else:
    try:
        file_handle = open('./files/' + file_name)

        count = 0

        for line in file_handle:
            line = line.rstrip()  # remove newline character from line
            if line.startswith('Subject'):
                count = count + 1

        print(str(file_name), 'has:', str(count), 'subject lines')

    except FileNotFoundError:
        print('File cannot be opened: ', file_name)
