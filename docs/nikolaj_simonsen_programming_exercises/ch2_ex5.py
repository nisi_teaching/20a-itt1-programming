# Exercise 5: Write a program which prompts the user for a Celsius tem-
# perature, convert the temperature to Fahrenheit, and print out the
# converted temperature.

# research: https://www.rapidtables.com/convert/temperature/how-celsius-to-fahrenheit.html
# T(°F) = T(°C) × 9/5 + 32
# or
# T(°F) = T(°C) × 1.8 + 32 

celcius = float(input('Enter temperature in °C: \n'))
fahrenheit = celcius * 1.8 + 32
print(str(celcius) + (' °C is equal to ') + str(fahrenheit) + ' °F')
