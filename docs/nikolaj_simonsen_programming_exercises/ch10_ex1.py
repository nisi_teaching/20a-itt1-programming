"""
Exercise 1: Revise a previous program as follows:
Read and parse the “From” lines and pull out the addresses from the line.
Count the number of messages from each person using a dictionary.

After all the data has been read, print the person with the most commits
by creating a list of (count, email) tuples from the dictionary.
Then sort the list in reverse order and print out the person who has the most
commits.

Sample Line:
From stephen.marquard@uct.ac.za Sat Jan 5 09:14:16 2008

Enter a file name: mbox-short.txt
cwen@iupui.edu 5
Enter a file name: mbox.txt
zqian@umich.edu 195
"""

file_name = input('Enter a file name: ')
try:
    mails_file = open("./files/" + file_name)
    mails_histogram = dict()
    largest = None
    max_receiver = ""

    for line in mails_file:
        if line.startswith("From: "):
            address = line[line.find(" ") + 1:].rstrip()
            if address not in mails_histogram:
                mails_histogram[address] = 1
            else:
                mails_histogram[address] += 1

    mails_file.close()

    lst = list()

    for name, count in list(mails_histogram.items()):
        lst.append((name, count))

    lst.sort(reverse=True)

    print(*lst[0])

except FileNotFoundError:
    print("File not found")