"""
Exercise 5: This program records the domain name (instead of the address)
where the message was sent from instead of who the mail came from (i.e., the whole email address).
At the end of the program, print out the contents of your dictionary.

Enter a file name: mbox-short.txt
{'media.berkeley.edu': 4, 'uct.ac.za': 6, 'umich.edu': 7,
'gmail.com': 1, 'caret.cam.ac.uk': 1, 'iupui.edu': 8}
"""
import matplotlib.pyplot as plt

file_name = input('Enter a file name: ')

if len(file_name) < 2:
    file_name = "mbox.txt"

try:
    mails_file = open('./files/' + file_name)
    mails_histogram = dict()

    for line in mails_file:
        if line.startswith("From: "):
            domain = line[line.find("@"):]
            if domain not in mails_histogram:
                mails_histogram[domain] = 1
            else:
                mails_histogram[domain] += 1

    mails_file.close()

    # print(mails_histogram)

    # convert dict to list and sort it
    sort_list = list()
    for sender, count in list(mails_histogram.items()):
        sort_list.append((count, sender))
    sort_list.sort()

    # lists for plot x and y axis
    sender_list = list()
    count_list = list()

    # add sorted list items to plot lists:
    for count, sender in sort_list:
        sender_list.append(sender)
        count_list.append(count)

    # show it in a horisontal barchart
    plt.style.use("seaborn")
    plt.barh(sender_list, count_list, color="#444444",  height=0.75, label="mails/day")
    plt.ylabel("Sender")
    plt.xlabel("Mails")
    plt.title("Mails a day chart")
    plt.legend()
    plt.tight_layout()
    plt.legend(bbox_to_anchor=(0.95, 0.2), bbox_transform=plt.gcf().transFigure)
    # plt.xscale("log")
    plt.show()

except FileNotFoundError:
    print("File not found")







