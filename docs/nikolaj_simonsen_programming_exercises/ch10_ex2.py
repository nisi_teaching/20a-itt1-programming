"""
Exercise 2: This program counts the distribution of the hour of the day
for each of the messages. You can pull the hour from the “From” line
by finding the time string and then splitting that string into parts using
the colon character.

Once you have accumulated the counts for each hour, print out the counts,
one per line, sorted by hour as shown below.

python timeofday.py
Enter a file name: mbox-short.txt
04 3
06 1
07 1
09 2
10 3
11 6
14 1
15 2
16 4
17 2
18 1
19 1
"""

file_name = input('Enter a file name: ')
if len(file_name) <=1:
    file_name = "mbox-short.txt"
try:
    mails_file = open("./files/" + file_name)
    hours_histogram = dict()
    largest = None
    max_receiver = ""

    for line in mails_file:
        if line.startswith("From "):
            words = line.split()
            hour = words[5][:2]
            # print(hour)
            if hour not in hours_histogram:
                hours_histogram[hour] = 1
            else:
                hours_histogram[hour] += 1
    mails_file.close()

    lst = list()

    for hour, count in list(hours_histogram.items()):
        lst.append((hour, count))

    lst.sort()

    for item in lst:
        print(*item)

except FileNotFoundError:
    print("File not found")