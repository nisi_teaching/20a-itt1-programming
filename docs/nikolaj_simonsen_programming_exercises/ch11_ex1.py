"""
Exercise 1: Write a simple program to simulate the operation of the
grep command on Unix. Ask the user to enter a regular expression and
count the number of lines that matched the regular expression:

$ python grep.py
Enter a regular expression: ^Author
mbox.txt had 1798 lines that matched ^Author

$ python grep.py
Enter a regular expression: ^X-
mbox.txt had 14368 lines that matched ^X-

$ python grep.py
Enter a regular expression: java$
mbox.txt had 4175 lines that matched java$
"""

import re

reg_exp = input('Enter a regular expression: ')
line_counter = 0

try:
    file = open('./files/mbox.txt')

    for line in file:
        line = line.rstrip() # remove \n
        reg_print = re.search(reg_exp, line)
        if re.search(reg_exp, line): #use reg-ex from input to search
            line_counter += 1 # count matching lines

    print('mbox.txt had', line_counter, "lines that matched", reg_exp)

except FileNotFoundError:
    "File not found"