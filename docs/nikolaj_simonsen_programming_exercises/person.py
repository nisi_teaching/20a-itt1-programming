"""
1.  Create the class in a Python file, name the class person()
2.  Add the following attributes
    1.  fullname (string)
    2.  birthdate (datetime formatted as YYYYMMDD)
    3.  address (list with streetname, streetnumber, city, postalcode, countrycode)
    4.  gender (male, female, other)
3.  Add the following methods
    1.  getfirstname(return str(firstname)
    2.  getlastname(return str(lastname)
    3.  getage(return int(age)
    4.  getaddress (only returnlist(streetname, streetnumber, city))
    5.  getfullinfo (return all info from the above methods as a dictionary)
4.  create at least 3 persons from your class and test your methods
"""

import datetime

class Person():

    #init function/constructor. Converts arguments to object instance variables
    def __init__(self, fullname, birthdate, address, gender): 
        self.fullname = fullname
        self.first_name = self.fullname.split(' ')[0]
        self.last_name = self.fullname.split(' ')[1]
        self.birthdate = birthdate
        self.address = address
        self.gender = gender

    # Methods
    def get_first_name(self):
        return self.first_name

    def get_last_name(self):
        return self.last_name

    def get_age(self):
        self.age = 0
        now = datetime.datetime.now()
        year = int(self.birthdate[0:4])
        month = int(self.birthdate[4:6])
        day = int(self.birthdate[6:8])
        b_date = datetime.datetime(year, month, day)
        age_days = (now - b_date).days
        age_years = age_days / 365
        return age_years

    def get_address(self):
        self.short_address = self.address[0:3]
        return  self.short_address

    # Returns an array
    def get_full_info(self):
        return {
            'first_name': self.get_first_name(), 
            'last_name': self.get_last_name(), 
            'age': self.get_age(), 
            'address': self.get_address()
            }

# Extending the Person Class
class ExtendedPerson(Person): 

    # Constructor. Uses the Super() function to initialize the parent class
    # https://www.pythonforbeginners.com/super/working-python-super-function
    def __init__(self, fullname, birthdate, address, gender, pet_type, pet_name, monthly_income):
        super().__init__(fullname, birthdate, address, gender)
        self.pet_type = pet_type
        self.pet_name = pet_name
        self.monthly_income = monthly_income

    # Methods
    def get_pet(self):
        return str(self.pet_type + " " + self.pet_name)

    def get_income(self):
        if self.monthly_income < 10000:
            return 'low'
        elif self.monthly_income > 10000 and self.monthly_income < 20000:
            return 'middle'
        elif self.monthly_income > 20000:
            return 'high'

    def get_full_info(self):
        return {
            'first_name': self.get_first_name(), 
            'last_name': self.get_last_name(), 
            'age': self.get_age(), 
            'address': self.get_address(),
            'pet': self.get_pet(),
            'income level': self.get_income()
            }

def main():
    
    address = ['Testvej', 2, 'Odense', '5000', 'DK'] #list of strings for address fields

    # object from Person class
    person1 = Person('Nikolaj Simonsen', '20000402', address, 'male')
    print('\n')
    print(person1.get_first_name(), person1.get_last_name(), person1.get_age(), *person1.get_address())
    
    print('\n')
    print(person1.get_full_info())
    print('\n')

    # Object from ExtendedPerson Class
    person_w_pet_1 = ExtendedPerson('John Doe', '19971114', address, 'male', 'cat', 'meow', 5000)
    # Print key value pairs from get_full_info() method (return type is a Dictionary)
    for (key, value) in person1.get_full_info().items():
        print(key+':', value)

# Only run main function if top level module (Main guard)
if __name__ == '__main__':
    main()
