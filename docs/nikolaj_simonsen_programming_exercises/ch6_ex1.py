"""
Exercise 1: Write a while loop that starts at the last character in the string
and works its way backwards to the first character in the string,
printing each letter on a separate line, except backwards.
"""

fruit = 'fruit'
index = len(fruit)  # 5


while index > 0:
    print(fruit[index-1])
    index -= 1

# print('\n')
# fruit_reverse = fruit[::-1]  # reversing the string with string slice
# for char in fruit_reverse:
#     print(char)
