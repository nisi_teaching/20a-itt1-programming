"""
Exercise 2: Write a program to prompt for a file name, and then read
through the file and look for lines of the form:
X-DSPAM-Confidence: 0.8475

When you encounter a line that starts with “X-DSPAM-Confidence:”
pull apart the line to extract the floating-point number on the line.

Count these lines and then compute the total of the spam confidence
values from these lines. When you reach the end of the file, print out
the average spam confidence.

Enter the file name: mbox.txt
Average spam confidence: 0.894128046745

Enter the file name: mbox-short.txt
Average spam confidence: 0.750718518519
"""

file_name = input('Enter a file name: ')

try:
    file_handle = open('./files/' + file_name)

    confidence_total = 0.0
    count = 0

    for line in file_handle:
        line = line.rstrip()  # remove newline character from line
        if line.startswith('X-DSPAM-Confidence:'):
            confidence = (line[line.find(': ') + 1:])  # find : in line and extract everything after it
            confidence_total += float(confidence)  # add line confidence to total confidence
            count = count + 1

    print('number of entries: ', count)
    print('Average spam confidence: ', round((confidence_total/count), 12))

except FileNotFoundError:
    print('File not found')
