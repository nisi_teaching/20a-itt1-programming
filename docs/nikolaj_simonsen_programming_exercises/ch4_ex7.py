"""
Exercise 7: Rewrite the grade program from the previous chapter
using a function called compute grade that takes a score
as its parameter and returns a grade as a string.

Score Grade
>= 0.9 A
>= 0.8 B
>= 0.7 C
>= 0.6 D
<  0.6 F

Enter score: 0.95
A
Enter score: perfect
Bad score
Enter score: 10.0
Bad score
Enter score: 0.75
C
Enter score: 0.5
F
Run the program repeatedly as shown above to test the various different values for
input.
"""


def main():

    try:
        score = float(input('Enter score: '))
        print(compute_grade(score))

    except ValueError:
        print('Enter a number please\n')
        main()


def compute_grade(_score):
    if _score > 1.0:
        return 'Bad score'
    elif _score >= 0.9:
        return 'A'
    elif _score >= 0.8:
        return 'B'
    elif _score >= 0.7:
        return 'C'
    elif _score >= 0.6:
        return 'D'
    elif _score < 0.6:
        return 'F'
    else:
        return 'Bad score'


if __name__ == '__main__':
    main()
