#imports
from person import ExtendedPerson

addresses = {
    'adr1': ['Testvej', 2, 'Odense', '5000', 'DK'], 
    'adr2': ['Dreamroad', 42, 'Heaven', '7913', 'ABOVE'], 
    'adr3': ['Wayout', 222, 'Las Vegas', '564487', 'USA']
    }

#Objects created from ExtendedPerson Class located in person.py
person_w_pet_1 = ExtendedPerson(
    'John Doe', 
    '19971114', 
    addresses['adr1'], 
    'male', 
    'cat', 
    'meow', 
    5000)

person_w_pet_2 = ExtendedPerson(
    'Marilyn Monroe', 
    '19570215', 
    addresses['adr2'], 
    'female', 
    'Lizard', 
    'Johnson', 
    30000)

person_w_pet_3 = ExtendedPerson(
    'Donald Trump', 
    '19681201', 
    addresses['adr3'], 
    'male', 
    'Melania', 
    'Trump', 
    15000)

# 3 objects from ExtendedPerson Class
print(person_w_pet_1.get_full_info())
print(person_w_pet_2.get_full_info())
print(person_w_pet_3.get_full_info())

# Loop over the dictionary returned from the get_full_info() method
for (key, value) in person_w_pet_1.get_full_info().items():
    print(key+':', value)
