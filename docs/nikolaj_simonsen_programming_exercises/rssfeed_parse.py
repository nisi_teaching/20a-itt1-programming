import feedparser
import json
from bs4 import BeautifulSoup

# RSS URL's
eal_itt = 'https://gitlab.com/EAL-ITT.atom'

# instantiate objects
rss_feed = feedparser.parse(eal_itt)

#print entire feed
# print(rss_feed.feed)

# print list of feed entries with json module to make it look nice
# print(json.dumps(rss_feed.entries, indent = 1))

# print first item in list of feed entries
# print(json.dumps(rss_feed.entries[0], indent = 1))

for entry in rss_feed.entries[0:5]:
    soup = BeautifulSoup(entry.summary_detail.value, 'html.parser')
    print(f'\nEntry id {entry.id}, Author: {entry.author}, Updated: {entry.updated}\nCommit message: {soup.div.p.text}')