---
Week: 46
tags:
- Dictionaries
- Programming challenge
---

# Exercises for ww46
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 9 knowledge QUIZ

This is a teacher paced quiz with questions about PY4E chapter 9 - Dictionaries

Go to [https://b.socrative.com/login/student/](https://b.socrative.com/login/student/) and write **simonsen7368** as the room number

Answer the first question and wait. When everyone have answered a question you will get the next question.

## Exercise 1 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 9 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

## Exercise 2 - Work on your own challenge

Like last week you now have time scheduled in class to work on you own challenge.

If you are still researching you are doing it wrong. The purpose of doing your own challenge is to get your hands dirty and write Python code.
The research should happen when you run into problems during coding.
The topic of the challenge is not that important, as long as you practice and write python code

Remember that OLA16 in next week is about your own challenge [https://eal-itt.gitlab.io/20a-itt1-programming/other-docs/20A_ITT1_PROGRAMMING_OLA16.html](https://eal-itt.gitlab.io/20a-itt1-programming/other-docs/20A_ITT1_PROGRAMMING_OLA16.html) 

\pagebreak