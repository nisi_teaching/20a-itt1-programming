---
Week: 38
tags:
- Git
- Gitlab
- Python interpreter
- Errors
- Debugging
- Variables
- Mnemonic naming
- Operators
---

# Exercises for ww38

All exercises are using cooperative learning structures.  
Each exercise specifies which structure to use.  
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - Python for everybody chapter 1 knowledge 

### Information

This exercise recaps what you have been reading in chapter 1 of Python for everybody.

### Exercise instructions

In your team make a shared online document and use **Round table** to share your understanding of:

* What is the difference between syntax errors, logic errors and semantic errors?
* What is input and output?
* What is sequential execution?
* Which 4 things should you do when debugging?

You have 10 minutes for all question, I will keep track of time and tell you when you have and 5 minutes left.

Use **circle of knowledge** to discuss your answers

## Exercise 1 - Python for everybody chapter 2 knowledge - part 1 

### Information

This exercise recaps parts of what you have been reading in chapter 2 of Python for everybody.

### Exercise instructions

In your team use the shared online document and **Round table** to share your understanding of what these operations do:

* What types of values is Python using?
* How would you check a values type in a Python program?
* What are variables?
* What are reserved words?
* What is a statement?

You have 20 minutes for all question, I will keep track of time and tell you when you have 10 and 5 minutes left.

## Exercise 2 - Python for everybody chapter 2 knowledge - part 2 

### Information

This exercise recaps parts of what you have been reading in chapter 2 of Python for everybody.

### Exercise instructions

use your shared online document and **meeting in the middle** to answer:

* What is the purpose of mnemonic naming? 
* Give at least 3 examples of mnemonic naming.
* Name pythons 6 operators and their syntax
* What is order of operations?
* What is concatenation?
* How do you ask the user for input in a Python program?
* How do you insert comments in a Python program? 

15 minutes to write answers for all questions  
15 minutes to explain  
10 minutes to discuss  

## Exercise 3 - Python for everybody chapter 2 exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 2 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak