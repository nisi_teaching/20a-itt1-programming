---
Week: 02
tags:
- Python quiz
- Your own challenge
---

# Exercises for ww02

## Exercise 0 - Python quiz

### Information

It is important to notice your own progress when learning. 
In week 44 we did the Python quiz at w3schools and we are going to do it again today.  

The purpose is to reflect on the progress you have been making, regarding Python, since week 44.

### Exercise instructions

Complete the Python quiz [https://www.w3schools.com/python/python_quiz.asp](https://www.w3schools.com/python/python_quiz.asp) 

You are not allowed to google for answers.
When the quiz is done, review your answers and compare with your answers from week 44.

You have 15 minutes.  

\pagebreak