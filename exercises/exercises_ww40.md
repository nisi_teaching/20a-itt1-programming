---
Week: 40
tags:
- Functions
- Pair programming
- Programming challenge
---

# Exercises for ww40
 
The cooperative learning structures can be found at [https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf](https://eal-itt.gitlab.io/cooperative_learning_structures/cooperative_learning_structures.pdf)

## Exercise 0 - PY4E chapter 4 knowledge (group)

### Information

This exercise recaps and shares knowledge, on a group level, about PY4E chapter 4.

### Exercise instructions

In your team discuss your understanding of:

1. What built-in Python functions do you know?
2. How are user functions defined in Python?
3. What are function parameters and arguments?
4. How do you return values from functions?
4. How do you call a function and assign the return value to a variable? 

Agree in your team on answers and complete the quiz at: [https://forms.gle/7VKQYAebsezgXSzR9](https://forms.gle/7VKQYAebsezgXSzR9)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

Note your answers for use in exercise 1

\pagebreak


## Exercise 1 - Pair programming challenge

### Information

In pairs of two students - use **Pair programming** to solve the `Write a function` challenge at hackerrank.

Use gitlab to share code between you. Remember the daily git operations:  

`git clone` Used to initially download a remote repository. Append a link to the command.  
`git pull` pulls changes from the remote repository to your local copy of the repository.   
`git add` Used to include files in a commit. Append a **.** to include all changes, or a filename for single files.  
`git commit -m "message"` commits added changes. Remember to replase message with something meaningful.  
`git push` pushes commits to the remote repository.  

A more thorough explanation is at [https://eal-itt.gitlab.io/gitlab_daily_workflow/index.html#2](https://eal-itt.gitlab.io/gitlab_daily_workflow/index.html#2)

### Exercise instructions

1. Create an account on hackerrank [https://www.hackerrank.com/domains/python](https://www.hackerrank.com/domains/python)
2. Read about pair-programming [http://www.extremeprogramming.org/rules/pair.html](http://www.extremeprogramming.org/rules/pair.html)
3. Solve the hello world challenge to get familiar with hackerrank [https://www.hackerrank.com/challenges/py-hello-world/problem](https://www.hackerrank.com/challenges/py-hello-world/problem) 
4. Use pair programming to solve the Write a function challenge [https://www.hackerrank.com/challenges/write-a-function/problem]()  
**All test cases must be passed when submitting your code**

Switch roles every 10 minutes, use a stopwatch to keep track of time.

You have until lunch to solve the `Write a function` challenge.  

If you finish before time proceed to the Python if-else challenge [https://www.hackerrank.com/challenges/py-if-else/problem](https://www.hackerrank.com/challenges/py-if-else/problem)   

**TIP** If you need an overview you can make a flowchart

## Exercise 2 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 4 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**