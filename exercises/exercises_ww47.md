---
Week: 47
tags:
- Tuple
- Matplotlib
- Visualization
---

# Exercises for ww47

## Exercise 0 - PY4E chapter 10 knowledge (group) 

### Information

In your team discuss your understanding of:

1. How can you create a tuple?
2. Can you change tuples after instantiation?
3. How does the slice operator work on tuples?
4. How do you compare tuples?
5. How does sort work on a tuple?   
6. How do you convert a dictionary to a list of tuples?
7. How do you sort a dictionary into a list ordered by value using tuples

### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/sETfaag4cQwTSoTi8](https://forms.gle/sETfaag4cQwTSoTi8)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## Exercise 1 - Matplotlib visualization

### Information

This exercise is about using a 3rd party module called matplotlib [https://matplotlib.org/](https://matplotlib.org/). It is a library for visualizing data in various diagrams and charts (line, bar, pie etc.)

Since we have been making histograms in the exercises for chapter 9 we will use those as the data input for the charts.

We will use the matplotlib video series [https://www.youtube.com/playlist?list=PL-osiE80TeTvipOqomVEeZ1HRrcEvtZB_](https://www.youtube.com/playlist?list=PL-osiE80TeTvipOqomVEeZ1HRrcEvtZB_) by Corey Schafer

### Exercise instructions

1. install matplotlib in your pycharm project [https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html](https://www.jetbrains.com/help/pycharm/installing-uninstalling-and-upgrading-packages.html)  
Currently there is an error in matplotlib, you might need to uninstall numpy with `pip uninstall numpy` and the re-install numpy with `pip install numpy==1.19.3` to make matplotlib work. 
2. Make a line chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 [https://youtu.be/UO98lJQ3QGI](https://youtu.be/UO98lJQ3QGI) just skip the install part if you are on pycharm (start at 2:00)  
Important! matplotlib uses seperate lists for the x and y axis.  
ch9_ex2 and ch9_ex5 will give you a dictionary that you have to convert into one list containing the keys and another list containing the values.  
3. Make a bar chart of the data from PY4E ch9_ex2 or ch9_ex5 following video1 [https://youtu.be/UO98lJQ3QGI](https://youtu.be/UO98lJQ3QGI)  

**Bonus challenge** Make a chart of choice from ch10_Ex3 

## Exercise 2 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 10 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak