---
Week: 44
tags:
- Programming challenge
- Parsing
- Files
- Exception handling
---

# Exercises for ww44

## Exercise 0 - PY4E chapter 7 knowledge (group)

### Information

In your team discuss your understanding of:  

* What is a file handle ?
* How do you count lines in a file ?
* How do you search through a file ?
* What does the `rstrip` method do ?
* What happens if you try to open a non existing file ?
* How do you write to a file ?


### Exercise instructions

Agree in your team on answers and complete the quiz at: [https://forms.gle/i3wKFMXH19Xb7goX9](https://forms.gle/i3wKFMXH19Xb7goX9)

You have 30 minutes to complete exercise 0

*Remember to note your answers in your team's shared document*

## Exercise 1 - Python quiz

It is important to notice your own progress when learning. This exercise will help you do that. 

### Exercise instructions

Complete the Python quiz [https://www.w3schools.com/python/python_quiz.asp](https://www.w3schools.com/python/python_quiz.asp) 

You are allowed to google for answers, but remember to finish in time.  
When the quiz is done, review your answers and note the ones you couldn't answer, we will revisit the quiz later in the semester.

You have 30 minutes.  

\pagebreak

## Exercise 2 - Find your own challenge

One of the best ways to get motivated learning new things is to find something you want to accomplish, in this case by using Python.  
I want you to find somtehing you want to accomplish using Python, something you would like to be able to code!  

Don't be afraid to pick something that you dont't know how to do with Python yet, you will learn while you do it and I will help you.

Examples:  
* Read your google calendar from a python script
* Make your own electronics calculator
* Count how many lines of Python code you have written
* Track which websites you spend time visiting
* Write a game
* A discord bot
* An element bot

You can also search online for challenges:  
realpython [https://realpython.com/what-can-i-do-with-python/](https://realpython.com/what-can-i-do-with-python/)  
codementor [https://www.codementor.io/ilyaas97/6-python-projects-for-beginners-yn3va03fs](https://www.codementor.io/ilyaas97/6-python-projects-for-beginners-yn3va03fs)  
codeclubprojects [https://codeclubprojects.org/en-GB/python/](https://codeclubprojects.org/en-GB/python/)

For the above i just searched google for "python projects"

You have 30 minutes to find a challenge. At 13:00 we will do a round on class where you present your chosen challenge.
Please document your own challenge as a seperate project in the gitlab group [https://gitlab.com/20a-itt1-programming-exercises](https://gitlab.com/20a-itt1-programming-exercises) remember to give the project a mnemonic name!  
In the project write a brief overview of the challenge you have chosen.  

In week 47 there will be an OLA about your own challenge [https://eal-itt.gitlab.io/20a-itt1-programming/other-docs/20A_ITT1_PROGRAMMING_OLA16.html](https://eal-itt.gitlab.io/20a-itt1-programming/other-docs/20A_ITT1_PROGRAMMING_OLA16.html)

## Exercise 3 - Python for everybody chapter exercises

### Information

Weekly programming exercises from PY4E chapters.

### Exercise instructions

Complete chapter 7 exercises, in Python For Everybody.
**remember to document in your gitlab programming project with seperate .py files. Suggested filename syntax: chX_exX.py**

\pagebreak