---
Week: 37
tags:
- Python
- Gitlab
- Git
---

# Exercises for ww37


## Exercise 0 - Install Python + Hello World

### Information

This exercise guides you on how to install python on your computer and how to write your first python program.

### Exercise instructions

1. Download the latest version of [Python](https://www.python.org/downloads/)
2. Open the installation file and follow the installer instructions  
![python installer](install_py.PNG "Python installer")  

3. Check that Python is installed by opening a `CMD` [prompt](https://www.lifewire.com/how-to-open-command-prompt-2618089) and write `py --version`    
If sucess you should see the python version in the `CMD` prompt  
![python install success](cmd_success.PNG "python install success")

4. Watch the video [Hello World in Python](https://youtu.be/KOdfpbnWLVo) to get background information on the rest of the exercise.

5. In `CMD` prompt write `python` to enter the [REPL](https://pythonprogramminglanguage.com/repl/) which is Pythons interactive shell. REPL is short for Read, Eval, Print and Loop.  
![REPL](REPL.PNG "REPL")

6. Write  
```python 
print ("Hello World")
``` 
and press enter  
![Hello World](hello_world.PNG "Hello World")  
If you see Hello World printed in REPL congratulations! You have written your first Python program.

Your program is not saved to the harddrive and can only be executed one time.
To write your program to the harddrive you need to make a python script. python scripts has the extension .py

7. Exit REPL by writing `quit()` and press enter

8. Create a folder on your computer called "`full_name`_programming_exercises"  
**hint** replace `full_name` with your first and last name in lower case,  
ie. `nikolaj_simonsen_programming_exercises`  
Remember the underscores to avoid white space in the name.

10. Click the windows start button and enter `idle` to find the Integrated Development Environment (IDE) that is installed with Python. Open it by clicking the icon.  

10. Create a new file from the file menu  
![IDLE](IDLE.PNG "IDLE")

11. Save the file as `000_hello_world.py` in your "`full_name`_programming_exercises" folder  

12. In `000_hello_world.py` write 
```python
print ("Hello World")
``` 
13. Save `000_hello_world.py` using the file menu 

13. Press `F5` to run the python script  
![hello world in idle](idle_hello_world.PNG "hello world in idle")

14. Open a `CMD` prompt from within your "`full_name`_programming_exercises" folder by navigating to the folder and write `CMD` in the address bar followed by enter  
![cmd prompt from folder](cmd_folder.PNG "cmd prompt from folder")

15. Execute `000_hello_world.py` with the command `python 000_hello_world.py`  
    This tells windows to run your script in Python  
    ![hello world in CMD](cmd_hello_world.PNG "hello world in CMD")

    You should see Hello World as the output

\pagebreak