---
layout: post
title: Hvad er dette?
date: 2019-12-04 13:32:20 +0300
description: intro besked
#img: i-rest.jpg # Add image post (optional)
#fig-caption: # Add figcaption (optional)
#tags: [Holidays, Hawaii]
---


Dette er første forsøg på en bedre hjemmeside til fag.

Vi har:

* Ugeplaner som [html]({{site.baseurl}}/weekly-plans)
* Opgaver som [html]({{site.baseurl}}/exercises)
* Ugeplaner og opgaver som [pdf filer]({{site.baseurl}}/pdf)
* [Tags]({{site.baseurl}}/tags)
